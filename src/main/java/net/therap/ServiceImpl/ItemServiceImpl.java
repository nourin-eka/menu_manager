package net.therap.ServiceImpl;

import net.therap.Service.ItemService;
import net.therap.dao.ItemTypeDayDao;
import net.therap.dao.ItemsDao;
import net.therap.daoimpl.ItemTypeDayDaoImpl;
import net.therap.daoimpl.ItemsDaoImpl;
import net.therap.domain.ItemTypeDay;
import net.therap.domain.Items;

import java.util.ArrayList;
import java.util.List;

/**
 * @author nourin
 * @since 11/15/16
 */
public class ItemServiceImpl implements ItemService{

    private ItemTypeDayDao itemTypeDayDao = new ItemTypeDayDaoImpl();
    private ItemsDao itemsDao = new ItemsDaoImpl();

    @Override
    public List<Items> getItemList(int typeId, int dayId) {
        List<Items> itemList = new ArrayList<>();
        List<ItemTypeDay> itemTypeDayList = itemTypeDayDao.getList(new ItemTypeDay(typeId,dayId));
        for(ItemTypeDay itemTypeDay : itemTypeDayList) {
            itemList.add(itemsDao.getItemById(new Items(itemTypeDay.getItemId())));
        }
        return itemList;
    }
}
