package net.therap.ServiceImpl;

import net.therap.Enums.MenuOptionEnum;
import net.therap.Service.ItemService;
import net.therap.Service.MenuService;
import net.therap.dao.ItemTypeDayDao;
import net.therap.dao.ItemsDao;
import net.therap.dao.TypeDao;
import net.therap.dao.WeekDaysDao;
import net.therap.daoimpl.ItemTypeDayDaoImpl;
import net.therap.daoimpl.ItemsDaoImpl;
import net.therap.daoimpl.TypeDaoImpl;
import net.therap.daoimpl.WeekDaysDaoImpl;
import net.therap.domain.ItemTypeDay;
import net.therap.domain.Items;
import net.therap.domain.Type;
import net.therap.domain.WeekDays;

import java.util.List;

/**
 * @author nourin
 * @since 11/14/16
 */
public class MenuServiceImpl implements MenuService {
    private final String[] WEEKDAYS = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday"};
    private final WeekDaysDao weekDaysDao = new WeekDaysDaoImpl();
    private final TypeDao typeDao = new TypeDaoImpl();
    private final ItemsDao itemsDao = new ItemsDaoImpl();
    private final ItemTypeDayDao itemTypeDayDao = new ItemTypeDayDaoImpl();
    private final ItemService itemService = new ItemServiceImpl();
    @Override
    public void showMenu(MenuOptionEnum type) {
        switch (type){
            case ALL_MENU:
                showBreakfastMenu();
                showLunchMenu();
                break;
            case BRKFST:
                showBreakfastMenu();
                break;
            case LUNCH:
                showLunchMenu();
                break;
            default: break;
        }
    }

    private void showBreakfastMenu() {
        System.out.println("BREAKFAST:\n");
        System.out.format("%10s%19s\n", "WEEKDAY", "DAILY MENU");
        System.out.format("%10s%19s\n", "-------", "----------");

        int breakfastId = typeDao.getTypeByName(new Type("breakfast")).getTypeId();

        for (String days : WEEKDAYS) {
            int weekDayId = weekDaysDao.getDayByName(new WeekDays(days)).getWeekDayId();
            List<Items> itemsList = itemService.getItemList(breakfastId,weekDayId);
            System.out.format("%10s", days);
            String listStr = "";

            for (int i = 0; i < itemsList.size(); i++) {
                listStr += itemsList.get(i).getItemName();
                if(i != itemsList.size() - 1) listStr += ", ";
            }
            System.out.format("%25s\n", listStr);
        }

    }

    private void showLunchMenu() {
        System.out.println("\n\nLUNCH:\n");
        System.out.format("%10s%19s\n", "WEEKDAY", "DAILY MENU");
        System.out.format("%10s%19s\n", "-------", "----------");
        int lunchId = typeDao.getTypeByName(new Type("lunch")).getTypeId();
        for (String days : WEEKDAYS) {
            int weekDayId = weekDaysDao.getDayByName(new WeekDays(days)).getWeekDayId();
            List<Items> itemsList = itemService.getItemList(lunchId,weekDayId);

            System.out.format("%10s", days);
            String listStr = itemsList.toString();
            listStr = listStr.substring(1, listStr.length() - 1);
            System.out.format("%25s\n", listStr);
        }

    }

    @Override
    public void addToMenu(String day, List<Items> itemsList, MenuOptionEnum menuType) {

        int dayId = weekDaysDao.getDayByName(new WeekDays(day)).getWeekDayId();
        int typeId = 0;
        if(menuType.equals(MenuOptionEnum.BRKFST)) {
            typeId = typeDao.getTypeByName(new Type("breakfast")).getTypeId();
        }else {
            typeDao.getTypeByName(new Type("lunch")).getTypeId();
        }

        for (Items item : itemsList) {
            Items domain = itemsDao.getItemByName(item);
            if(domain == null) {
                domain = itemsDao.addItem(item);
            }
            itemTypeDayDao.addItemTypeDay(new ItemTypeDay(domain.getIdItems(), typeId, dayId));
        }
    }

    @Override
    public void deleteFromMenu(String day, MenuOptionEnum menuType) {
        int dayId = weekDaysDao.getDayByName(new WeekDays(day)).getWeekDayId();
        int typeId = 0;
        if(menuType.equals(MenuOptionEnum.BRKFST)) {
            typeId = typeDao.getTypeByName(new Type("breakfast")).getTypeId();
        }else {
            typeDao.getTypeByName(new Type("lunch")).getTypeId();
        }
        itemTypeDayDao.deleteByTypeAndDay(new ItemTypeDay(typeId, dayId));
    }

}
