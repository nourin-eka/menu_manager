package net.therap.menumanager;

import net.therap.Enums.MenuOptionEnum;

import java.util.ArrayList;
import java.util.List;

/**
 * @author nourin
 * @since 11/13/16
 */
public class MenuManager {

    public static void main(String[] args) {
        new ConsoleMenu().start();
    }
}
