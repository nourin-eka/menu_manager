package net.therap.menumanager;

import net.therap.Enums.MenuOptionEnum;
import net.therap.Service.MenuService;
import net.therap.ServiceImpl.MenuServiceImpl;
import net.therap.domain.Items;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author nourin
 * @since 11/13/16
 */
public class ConsoleMenu {

    private final Scanner scanner = new Scanner(System.in);
    private final MenuService menuService = new MenuServiceImpl();
    private final String[] WEEKDAYS = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday"};

    public ConsoleMenu() {

    }

    public void start() {

        int choice;
        choice = showAndGetOption();
        performOption(getChoiceEnum(choice));
        System.out.println();
    }

    private void performOption(MenuOptionEnum choice){
        switch (choice){
            case ALL_MENU:
                menuService.showMenu(MenuOptionEnum.ALL_MENU);
                break;
            case BRKFST:
                menuService.showMenu(MenuOptionEnum.BRKFST);
                break;
            case LUNCH:
                menuService.showMenu(MenuOptionEnum.LUNCH);
                break;
            case UPDATE_BRKFST:
                showEditMenu("Breakfast", MenuOptionEnum.BRKFST);
                break;
            case UPDATE_LUNCH:
                showEditMenu("Lunch", MenuOptionEnum.LUNCH);
                break;
            default:
                System.out.println("Exiting the program.....");
                break;
        }
        if(!choice.equals(MenuOptionEnum.EXIT)) {
            start();
        }
    }

    public void showEditMenu(String type, MenuOptionEnum optionEnum) {
        System.out.format("%5s\n", "1. Add to " + type + " Menu");
        System.out.format("%5s\n", "2. Update " + type + " Menu");
        System.out.format("%5s\n", "3. Delete from " + type + " Menu");

        System.out.println("Enter an integer value as your choice:");
        int choice = scanner.nextInt();
        if(choice == 1) {
            addToMenu(optionEnum);
        }else if(choice == 2) {
            updateMenu(optionEnum);
        }else if(choice == 3) {
            deleteFromMenu(optionEnum);
        }
    }

    public void deleteFromMenu(MenuOptionEnum menuType) {
        String choosenDay = choiceDay();
        menuService.deleteFromMenu(choosenDay, menuType);
    }

    public void addToMenu(MenuOptionEnum menuType) {
        String choosenDay = choiceDay();
        List<Items> itemsList = getItems();
        menuService.addToMenu(choosenDay, itemsList, menuType);
    }

    public void updateMenu(MenuOptionEnum menuType) {
        String choosenDay = choiceDay();
        menuService.deleteFromMenu(choosenDay, menuType);
        List<Items> itemsList = getItems();
        menuService.addToMenu(choosenDay, itemsList, menuType);
    }

    private List<Items> getItems() {
        ArrayList<Items> itemsList = new ArrayList<>();
        System.out.println("Enter the number of items: ");
        int itemNumber = scanner.nextInt();
        for (int i = 0; i < itemNumber; i++) {
            System.out.print("Item #" + (i+1) + ": ");
            itemsList.add(new Items(scanner.next()));
        }
        return itemsList;
    }

    private String choiceDay() {
        System.out.println("Choice WeekDay. Enter your choice number: ");
        for(int i = 0; i < WEEKDAYS.length; i++) {
            System.out.println((i+1) + ". " + WEEKDAYS[i]);
        }
        int choice = scanner.nextInt();
        return WEEKDAYS[choice-1];
    }

    private int showAndGetOption() {
        System.out.format("%5s\n", "1. Show All Menu");
        System.out.format("%5s\n", "2. Show BreakFast Menu");
        System.out.format("%5s\n", "3. Show Lunch Menu");
        System.out.format("%5s\n", "4. Update BreakFast Menu");
        System.out.format("%5s\n", "5. Update Lunch Menu");
        System.out.format("%5s\n", "6. Exit Program");
        System.out.println();
        System.out.println("Enter an integer value as your choice:");
        return scanner.nextInt();
    }

    private MenuOptionEnum getChoiceEnum(int choice) {

        if (choice == MenuOptionEnum.ALL_MENU.getValue()) {
            return MenuOptionEnum.ALL_MENU;
        } else if (choice == MenuOptionEnum.BRKFST.getValue()) {
            return MenuOptionEnum.BRKFST;
        } else if (choice == MenuOptionEnum.LUNCH.getValue()) {
            return MenuOptionEnum.LUNCH;
        } else if (choice == MenuOptionEnum.UPDATE_BRKFST.getValue()) {
            return MenuOptionEnum.UPDATE_BRKFST;
        } else if (choice == MenuOptionEnum.UPDATE_LUNCH.getValue()) {
            return MenuOptionEnum.UPDATE_LUNCH;
        } else if (choice == MenuOptionEnum.EXIT.getValue()) {
            return MenuOptionEnum.EXIT;
        }
        return MenuOptionEnum.INVALID;
    }
}