package net.therap.domain;

/**
 * @author nourin
 * @since 11/13/16
 */
public class Items {

    private int itemId;
    private String itemName;

    public Items() {
    }

    public Items(int itemId, String itemName) {
        this.itemId = itemId;
        this.itemName = itemName;
    }

    public Items(String itemName) {
        this.itemName = itemName;
    }

    public Items(int itemId) {
        this.itemId = itemId;
    }

    public int getIdItems() {
        return itemId;
    }

    public void setIdItems(int itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }
}
