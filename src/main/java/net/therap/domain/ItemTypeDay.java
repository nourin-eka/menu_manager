package net.therap.domain;

/**
 * @author nourin
 * @since 11/13/16
 */
public class ItemTypeDay {

    private int itemTypeDayid;
    private int itemId;
    private int typeId;
    private int dayId;

    public ItemTypeDay() {
    }

    public ItemTypeDay(int itemTypeDayid, int itemId, int typeId, int dayId) {
        this.itemTypeDayid = itemTypeDayid;
        this.itemId = itemId;
        this.typeId = typeId;
        this.dayId = dayId;
    }

    public ItemTypeDay(int itemId, int typeId, int dayId) {
        this.itemId = itemId;
        this.typeId = typeId;
        this.dayId = dayId;
    }
    public ItemTypeDay(int typeId, int dayId) {
        this.typeId = typeId;
        this.dayId = dayId;
    }

    public int getItemTypeDayid() {
        return itemTypeDayid;
    }

    public void setItemTypeDayid(int itemTypeDayid) {
        this.itemTypeDayid = itemTypeDayid;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public int getDayId() {
        return dayId;
    }

    public void setDayId(int dayId) {
        this.dayId = dayId;
    }
}
