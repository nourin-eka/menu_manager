package net.therap.domain;

/**
 * @author nourin
 * @since 11/13/16
 */
public class Type {

    private int typeId;
    private String itemTypeName;

    public Type() {
    }

    public Type(int typeId) {
        this.typeId = typeId;
    }

    public Type(String itemTypeName) {
        this.itemTypeName = itemTypeName;
    }

    public Type(int typeId, String itemTypeName) {
        this.typeId = typeId;
        this.itemTypeName = itemTypeName;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public String getItemTypeName() {
        return itemTypeName;
    }

    public void setItemTypeName(String itemTypeName) {
        this.itemTypeName = itemTypeName;
    }
}
