package net.therap.domain;

/**
 * @author nourin
 * @since 11/13/16
 */
public class WeekDays {

    private int weekDayId;
    private String dayName;

    public WeekDays() {
    }

    public WeekDays(int weekDayId) {
        this.weekDayId = weekDayId;
    }

    public WeekDays(String dayName) {
        this.dayName = dayName;
    }

    public WeekDays(int weekDayId, String dayName) {
        this.weekDayId = weekDayId;
        this.dayName = dayName;
    }

    public int getWeekDayId() {
        return weekDayId;
    }

    public void setWeekDayId(int weekDayId) {
        this.weekDayId = weekDayId;
    }

    public String getDayName() {
        return dayName;
    }

    public void setDayName(String dayName) {
        this.dayName = dayName;
    }
}
