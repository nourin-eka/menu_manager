package net.therap.dbconnection;

import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author nourin
 * @since 11/15/16
 */
public class DBConnection {

    private static DBConnection dbConnectionInstance;
    private static Connection connectionToDB;
    private static ResultSet resultSet = null;
    private static Statement statement = null;
    private static Connection connection = null;
    private static final String DB_URL = "jdbc:mysql://localhost:3306/menu_database";
    private static final String DB_USER = "root";
    private static final String DB_PASS = "nourineka";

    private DBConnection() {

    }

    private static DBConnection getInstance() {
        if (dbConnectionInstance == null) {
            return dbConnectionInstance = new DBConnection();
        }
        return dbConnectionInstance;
    }

    public static Connection dbConnect() {
        try {
            if (connection == null) {
                Class.forName("com.mysql.jdbc.Driver");
                connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
            }
            return connection;
        } catch (Exception e) {
            System.out.println("DBConnection.java : " + e);
            e.printStackTrace();
            return null;
        }
    }

    public static void close() {
        try {
            if (connectionToDB != null)
                connectionToDB.close();
            if (statement != null)
                statement.close();
            if (resultSet != null)
                resultSet.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
