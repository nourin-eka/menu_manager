package net.therap.Enums;

/**
 * @author nourin
 * @since 11/14/16
 */
public enum MenuOptionEnum {
    ALL_MENU(1),
    BRKFST(2),
    LUNCH(3),
    UPDATE_BRKFST(4),
    UPDATE_LUNCH(5),
    EXIT(6),
    INVALID(7);

    private int value;

    MenuOptionEnum(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setOptionValue(int value) {
        this.value = value;
    }
};

