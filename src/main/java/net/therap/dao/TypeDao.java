package net.therap.dao;

import net.therap.domain.Type;

/**
 * @author nourin
 * @since 11/14/16
 */
public interface TypeDao {
    public Type getTypeByName(Type type);
    public Type getTypeById(Type type);
}
