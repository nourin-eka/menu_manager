package net.therap.dao;

import net.therap.domain.WeekDays;

/**
 * @author nourin
 * @since 11/14/16
 */
public interface WeekDaysDao {
    public WeekDays getDayByName(WeekDays weekDay);
    public WeekDays getDayById(WeekDays weekDay);
}
