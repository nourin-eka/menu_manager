package net.therap.dao;

import net.therap.domain.ItemTypeDay;

import java.util.List;

/**
 * @author nourin
 * @since 11/14/16
 */
public interface ItemTypeDayDao {

    public List<ItemTypeDay> getList(ItemTypeDay itemTypeDay);
    public ItemTypeDay addItemTypeDay(ItemTypeDay itemTypeDay);
    public void deleteByTypeAndDay(ItemTypeDay itemTypeDay);
}
