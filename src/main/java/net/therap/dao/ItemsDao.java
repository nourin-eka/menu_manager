package net.therap.dao;

import net.therap.domain.Items;

/**
 * @author nourin
 * @since 11/14/16
 */
public interface ItemsDao {

    public Items getItemById(Items item);
    public Items getItemByName(Items item);
    public Items addItem(Items item);
}
