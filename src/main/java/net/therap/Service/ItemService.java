package net.therap.Service;

import net.therap.domain.Items;

import java.util.List;

/**
 * @author nourin
 * @since 11/15/16
 */
public interface ItemService {
    public List<Items> getItemList(int typeId, int dayId);
}
