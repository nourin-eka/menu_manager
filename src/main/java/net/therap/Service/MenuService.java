package net.therap.Service;

import net.therap.Enums.MenuOptionEnum;
import net.therap.domain.Items;

import java.util.List;

/**
 * @author nourin
 * @since 11/14/16
 */
public interface MenuService {
    public void showMenu(MenuOptionEnum type);
    public void addToMenu(String day, List<Items> itemsList, MenuOptionEnum menuType);
    public void deleteFromMenu(String day, MenuOptionEnum menuType);
}
