package net.therap.daoimpl;

import net.therap.dao.TypeDao;
import net.therap.dbconnection.DBConnection;
import net.therap.domain.Type;
import net.therap.domain.WeekDays;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author nourin
 * @since 11/14/16
 */
public class TypeDaoImpl implements TypeDao {
    private final String typeIdColumn = "idtype";
    private final String typeNameColumn = "item_type_name";

    @Override
    public Type getTypeByName(Type type) {
        Type t = new Type();
        try {
            String query = "select * from type where item_type_name like ?";
            PreparedStatement preparedStatement = DBConnection.dbConnect().prepareStatement(query);
            preparedStatement.setString(1, type.getItemTypeName().toLowerCase());
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                String typeId = rs.getString(typeIdColumn);
                String typeName = rs.getString(typeNameColumn);
                t.setTypeId(Integer.parseInt(typeId));
                t.setItemTypeName(typeName);
            }
            return t;
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            DBConnection.close();
        }
        return t;
    }

    @Override
    public Type getTypeById(Type type) {
        Type t = new Type();
        try {
            String query = "select * from type where idtype = ?";
            PreparedStatement preparedStatement = DBConnection.dbConnect().prepareStatement(query);
            preparedStatement.setInt(1, type.getTypeId());
            ResultSet rs = preparedStatement.executeQuery(query);
            if (rs.next()) {
                String typeId = rs.getString(typeIdColumn);
                String typeName = rs.getString(typeNameColumn);
                t.setTypeId(Integer.parseInt(typeId));
                t.setItemTypeName(typeName);
            }
            return t;
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            DBConnection.close();
        }
        return t;
    }
}
