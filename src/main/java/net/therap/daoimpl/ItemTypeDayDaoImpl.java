package net.therap.daoimpl;

import net.therap.dao.ItemTypeDayDao;
import net.therap.dbconnection.DBConnection;
import net.therap.domain.ItemTypeDay;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * @author nourin
 * @since 11/14/16
 */
public class ItemTypeDayDaoImpl implements ItemTypeDayDao{

    private final String idColumn = "iditem_type_day";
    private final String itemIdColumn = "item_id";
    private final String typeIdColumn = "type_id";
    private final String dayIdColumn = "day_id";

    @Override
    public List<ItemTypeDay> getList(ItemTypeDay itemTypeDay) {
        List<ItemTypeDay> list = new ArrayList<>();
        try {
            String query = "select * from item_type_day where type_id = ? and day_id = ?;";
            PreparedStatement preparedStatement = DBConnection.dbConnect().prepareStatement(query);
            preparedStatement.setInt(1, itemTypeDay.getTypeId());
            preparedStatement.setInt(2, itemTypeDay.getDayId());
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                String id = rs.getString(idColumn);
                String itemId = rs.getString(itemIdColumn);
                String typeId = rs.getString(typeIdColumn);
                String dayId = rs.getString(dayIdColumn);

                ItemTypeDay domain = new ItemTypeDay();
                domain.setItemTypeDayid(Integer.parseInt(id));
                domain.setItemId(Integer.parseInt(itemId));
                domain.setTypeId(Integer.parseInt(typeId));
                domain.setDayId(Integer.parseInt(dayId));

                list.add(domain);
            }
            return list;
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            DBConnection.close();
        }
        return list;
    }

    @Override
    public ItemTypeDay addItemTypeDay(ItemTypeDay itemTypeDay) {
        try {
            String query = "INSERT INTO item_type_day(item_id, type_id, day_id) VALUES (?,?,?)";
            PreparedStatement preparedStatement = DBConnection.dbConnect().prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setInt(1, itemTypeDay.getItemId());
            preparedStatement.setInt(2, itemTypeDay.getTypeId());
            preparedStatement.setInt(3, itemTypeDay.getDayId());

            preparedStatement.execute();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            if (rs.next()) {
                itemTypeDay.setItemTypeDayid(rs.getInt(1));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            DBConnection.close();
        }
        return itemTypeDay;
    }

    @Override
    public void deleteByTypeAndDay(ItemTypeDay itemTypeDay) {
        try {
            String query = "delete from item_type_day where type_id = ? and day_id = ?";
            PreparedStatement preparedStatement = DBConnection.dbConnect().prepareStatement(query);
            preparedStatement.setInt(1, itemTypeDay.getTypeId());
            preparedStatement.setInt(2, itemTypeDay.getDayId());
            preparedStatement.execute();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            DBConnection.close();
        }
    }

}
