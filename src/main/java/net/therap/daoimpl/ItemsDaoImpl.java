package net.therap.daoimpl;

import net.therap.dao.ItemsDao;
import net.therap.dbconnection.DBConnection;
import net.therap.domain.Items;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author nourin
 * @since 11/14/16
 */
public class ItemsDaoImpl implements ItemsDao {

    private final String itemIdColumn = "iditems";
    private final String itemNameColumn = "item_name";

    @Override
    public Items getItemById(Items item) {
        Items domain = null;
        try {
            String query = "select * from items where iditems = ?";
            PreparedStatement preparedStatement = DBConnection.dbConnect().prepareStatement(query);
            preparedStatement.setInt(1, item.getIdItems());
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                domain = new Items();

                String itemId = rs.getString(itemIdColumn);
                String itemName = rs.getString(itemNameColumn);

                domain.setIdItems(Integer.parseInt(itemId));
                domain.setItemName(itemName);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            DBConnection.close();
        }
        return domain;
    }

    @Override
    public Items getItemByName(Items item) {
        Items domain = null;
        try {
            String query = "select * from items where item_name = ?";
            PreparedStatement preparedStatement = DBConnection.dbConnect().prepareStatement(query);
            preparedStatement.setString(1, item.getItemName().toLowerCase());
            ResultSet rs = preparedStatement.executeQuery(query);
            if (rs.next()) {
                domain = new Items();

                String itemId = rs.getString(itemIdColumn);
                String itemName = rs.getString(itemNameColumn);

                domain.setIdItems(Integer.parseInt(itemId));
                domain.setItemName(itemName);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            DBConnection.close();
        }
        return domain;
    }

    @Override
    public Items addItem(Items item) {
        try {
            String query = "INSERT INTO items (item_name) VALUES (?)";
            PreparedStatement preparedStatement = DBConnection.dbConnect().prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, item.getItemName().toLowerCase());
            preparedStatement.execute();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            if(rs.next()) {
                item.setIdItems(rs.getInt(1));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            DBConnection.close();
        }
        return item;
    }
}
