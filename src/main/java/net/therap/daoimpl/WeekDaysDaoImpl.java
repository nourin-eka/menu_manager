package net.therap.daoimpl;

import net.therap.dao.WeekDaysDao;
import net.therap.dbconnection.DBConnection;
import net.therap.domain.WeekDays;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author nourin
 * @since 11/14/16
 */
public class WeekDaysDaoImpl implements WeekDaysDao {

    private String weekDayIdColumn = "idweek_days";
    private String weekDayNameColumn = "day_name";
    @Override
    public WeekDays getDayByName(WeekDays weekDay) {
        WeekDays day = new WeekDays();
        try {
            String query = "select * from week_days where day_name = ?";
            PreparedStatement preparedStatement = DBConnection.dbConnect().prepareStatement(query);
            preparedStatement.setString(1, weekDay.getDayName().toLowerCase());
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                String weekId = rs.getString(weekDayIdColumn);
                String dayName = rs.getString(weekDayNameColumn);
                day.setWeekDayId(Integer.parseInt(weekId));
                day.setDayName(dayName);
            }
            return day;
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            DBConnection.close();
        }
        return day;
    }

    @Override
    public WeekDays getDayById(WeekDays weekDay) {
        WeekDays day = new WeekDays();
        try {
            String query = "select * from week_days where idweek_days = ?";
            PreparedStatement preparedStatement = DBConnection.dbConnect().prepareStatement(query);
            preparedStatement.setInt(1,weekDay.getWeekDayId());
            ResultSet rs = preparedStatement.executeQuery(query);
            if (rs.next()) {
                String weekId = rs.getString(weekDayIdColumn);
                String dayName = rs.getString(weekDayNameColumn);
                day.setWeekDayId(Integer.parseInt(weekId));
                day.setDayName(dayName);
            }
            return day;
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            DBConnection.close();
        }
        return day;
    }
}
